| N_STUDENTS  | N_CLASSES  | TIME  | MEMORY |
|-------------|------------|-------|--------|
| 3 | 10 | 0.000000 seconds | 3.246094 Mbytes|
| 3 | 100 | 0.000000 seconds | 3.250000 Mbytes|
| 3 | 500 | 0.000000 seconds | 3.312500 Mbytes|
| 10 | 10 | 0.000000 seconds | 3.261719 Mbytes|
| 10 | 100 | 0.000000 seconds | 3.117188 Mbytes|
| 10 | 500 | 0.004000 seconds | 3.242188 Mbytes|
| 50 | 10 | 0.100000 seconds | 3.320312 Mbytes|
| 50 | 100 | 0.004000 seconds | 3.199219 Mbytes|
| 50 | 500 | 0.004000 seconds | 3.218750 Mbytes|
| 100 | 10 | 0.684000 seconds | 3.488281 Mbytes|
| 100 | 100 | 0.008000 seconds | 3.269531 Mbytes|
| 100 | 500 | 0.004000 seconds | 3.332031 Mbytes|
| 1000 | 10 | 708.796021 seconds | 17.273438 Mbytes|
| 1000 | 100 | 0.840000 seconds | 4.558594 Mbytes|
| 1000 | 500 | 0.296000 seconds | 4.195312 Mbytes|
