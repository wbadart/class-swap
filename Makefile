MAIN_EXE=class_swap
MAIN_OBJS=src/main.o src/constants.o src/database.o src/student.o src/generator.o src/menu.o

GEN_EXE=generate_random_scenario
GEN_OBJS=src/generator.o src/generate_random_scenario.o

CXX=g++
CXXFLAGS=-std=gnu++11

LINKER=g++
LINKERFLAGS=-std=gnu++11
LINKS=

TESTS=test-stability test-memory test-time

all: $(MAIN_EXE) $(GEN_EXE)

debug: CXXFLAGS=-std=gnu++11 -W -Wall -Wextra -W -O0 -ggdb3
debug: all

clean:
	rm $(MAIN_EXE) $(MAIN_OBJS) $(GEN_EXE) $(GEN_OBJS)

$(MAIN_EXE): $(MAIN_OBJS)
	$(LINKER) $(LINKERFLAGS) $^ $(LINKS) -o $@

$(GEN_EXE): $(GEN_OBJS)
	$(LINKER) $(LINKERFLAGS) $^ $(LINKS) -o $@

%.o: %.cpp *.hpp
	$(CXX) $(CXXFLAGS) -c $<

test: $(TESTS)

test-stability: all
	@echo "Testing pairing stability"
	@./$(GEN_EXE) 500 100 5 4 | ./$(MAIN_EXE) -r -c 1> /dev/null 2> /dev/null

test-memory: all
	@echo "Testing memory"
	@./$(GEN_EXE) 500 100 5 4 | valgrind --leak-check=full ./$(MAIN_EXE) 2>&1 | grep -v ERROR > /dev/null

test-time: all
	@echo "Testing time"
	@./$(GEN_EXE) 500 100 5 4 | ./measure ./$(MAIN_EXE) | tail -n 1 | awk '{ if ($$1 > 1.0) {print "Time limit exceeded"; exit 1} }'

