CONTRIBUTORS
============

### cbailey8
Bailey, Conrad

Built out the pipeline which transformed input student data into
operable student and class objects. Researched variations of the
core algorithm (Gale-Shapley). Created the program's interactive
mode.


### wbadart
Badart, Will

Built the web application (interface and server). Worked on project
setup and initial program scaffolds. Wrote the test suite and setup CI
pipeline.


### mkusters
Kusters, Madeline

Wrote data generation program. Worked on object domain and adapting
Gale-Shapley to fit our data structure. Created and ran benchmarks.
Created demo presentation.


### Everyone

Algorithm implementation and verification.
