#!/bin/sh

##
# benchmark.sh
#
# Test performace of core program.
#
# DEC 2016
##

EXE="class_swap"
GEN_EXE="generate_random_scenario"

N_STUDENTS_LIST="3 10 50 100 1000" # 10000"
N_CLASSES_LIST="10 100 500"

MAX_CLASSES=5
MAX_TRADES=4

# Make sure EXE exists
if [ ! -e ./$EXE ]; then
    echo "Building $EXE..." 1>&2
    make all 1>&2
    echo "DONE\n" 1>&2
fi


# Construct performance table
echo "| N_STUDENTS  | N_CLASSES  | TIME  | MEMORY |"
echo "|-------------|------------|-------|--------|"

for N_STUDENTS in `echo $N_STUDENTS_LIST`; do
    for N_CLASSES in `echo $N_CLASSES_LIST`; do
        OUTPUT=`./$GEN_EXE $N_CLASSES $N_STUDENTS $MAX_CLASSES $MAX_TRADES | ./measure ./$EXE -r 2> /dev/null`
        OUTPUT=`echo $OUTPUT | rev | cut -d ' ' -f -4 | rev`
        echo "| $N_STUDENTS | $N_CLASSES | `echo $OUTPUT | cut -d ' ' -f 1,2` | `echo $OUTPUT | cut -d ' ' -f 3,4`|"
    done
done

