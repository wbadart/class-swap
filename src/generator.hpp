#ifndef GENERATOR
#define GENERATOR

#include <iostream>
#include <random>
#include <chrono>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <string>
#include <set>

using namespace std;
void generate_scenario (ostream& stream, int numclasses, int numstudents, int maxclasses, int maxwantedclasses);

#endif
