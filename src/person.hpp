/**
 * person.hpp
 *
 * Interface file of Person class and its
 * children, Man and Woman.
 *
 * DEC 2016
 *
 */

#ifndef H_PERSON
#define H_PERSON

#include <deque>
#include <iostream>
#include <map>
#include <set>
#include <string>

class Person{
    public:

        Person(std::string n="", std::deque<std::pair<std::string, std::string>*>
                p=std::deque<std::pair<std::string, std::string>*>());
        std::deque<std::pair<std::string, std::string>*> preferences;
        std::string netid;

        void propose(Person);
        Person* proposal_target;

        void reject(void);
        std::set<Person*> proposals;

        static void print_set(std::ostream&, std::map<std::string, Person>);
        bool operator<(const Person& b) const{
            return netid < b.netid;
        }

    private:
};

#endif // H_PERSON

