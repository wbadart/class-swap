/**
 * constants.hpp
 *
 * Header containing program-wide
 * constants, mostly config stuff.
 *
 * DEC 2016
 */

#ifndef H_CONSTANTS
#define H_CONSTANTS

#include <string>

extern const std::string PROG_NAME;
extern const std::string OPTSTRING;

namespace MSG{
    extern const std::string usage;
};

namespace ERR{
    extern const std::string opt_not_recognized;
};

#endif // H_CONSTANTS

