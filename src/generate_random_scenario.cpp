#include <iostream>
#include <random>
#include <chrono>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <string>
#include <set>

#include "generator.hpp"

using namespace std;

int main (int argc, char * argv[]) {

  if (argc != 5) {
    cerr << "./generate_randomscenario <numclasses> <numstudents> <maxclassesperstudent> <maxwantedclasses>" << endl;
    exit(1);
  }



  int numclasses, numstudents, maxclasses, maxwantedclasses;
  //cin >> numclasses >> numstudents >> maxclasses;
  numclasses = atoi(argv[1]);
  numstudents = atoi(argv[2]);
  maxclasses = atoi(argv[3]);
  maxwantedclasses = atoi(argv[4]);

  if (maxwantedclasses > numclasses) {
    cerr << "maxwantedclasses exceeds numclasses" << endl;
    exit(1);
  }

  if (maxclasses > numclasses) {
    cerr << "maxclasses exceeds numclasses" << endl;
    exit(1);
  }

  generate_scenario(cout, numclasses, numstudents, maxclasses, maxwantedclasses);
}
