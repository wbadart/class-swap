/**
 * constants.cpp
 *
 * Initialization for constatnts.
 *
 * DEC 2016
 *
 */

#include "constants.hpp"

const std::string PROG_NAME = std::string("class_swap");

const std::string MSG::usage = {
R"(usage: class_swap [ OPTIONS ]
    Reads stdin for network of students and class preferences. Outputs pairs of
    valid and stable swaps.

    OPTIONS
    -h          Display this help message
    -m          Use the interactive menu to play around
    -c          Verify pairing stability
    -r          Read a scenario from stdin and output the swapped schedules
)"
};

const std::string ERR::opt_not_recognized = "ERR: option not recognized";

