#ifndef STUDENT_H
#define STUDENT_H

#include <vector>
#include <string>
#include <set>
#include <queue>
#include <map>
#include <iostream>
class Student;

class Course {
public:
  std::string crn;
  Student *owner;
  std::vector<Course *> received;
  Course * last_proposal = nullptr;

  size_t student_i = 0;
  size_t want_crn_i = 0;

  Course * get_next_pref();
  void propose_to(Course *woman);
  bool reject_all_but_best();
  void reject(Course *proposer);
  bool walk_reject_all_but_best();
  void walk_reject(Course *proposer);

  Course(Student *o);
};


class Student {
public:
  std::string netid;
  std::vector<Course> trade_aways;

  //       CRN          Course object list
  std::map<std::string, std::vector<Course *>> trade_fors; // A map ordered in a way we designate to reflect precedence would be best

  std::vector<std::string> want_crns; // use this to determine preference for now

  Student(std::string n, size_t num_has, size_t num_wants, Student *addr);
  Student();
  Course * has(std::string crn);
  void add_trade_for(Course *match);
  bool more_preferred(Course *a, Course *b);
};

#endif
