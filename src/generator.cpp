#include "generator.hpp"
#include <iomanip>

mt19937 random_generator{chrono::system_clock::now().time_since_epoch().count()};

void generate_scenario (ostream& stream, int numclasses, int numstudents, int maxclasses, int maxwantedclasses) {
  stream << numstudents << endl;
  vector<string> names;
  fstream babies;
  babies.open("baby_names.txt");

  string baby;
  while(babies >> baby) {
    names.push_back(baby);
  }

  for (int i = 0; i < numstudents; i++) {
    int rand = random_generator() % names.size();
    stream << names[rand] << " ";
    names.erase(names.begin()+rand);

    int has = (random_generator() % maxclasses) + 1;
    stream << has << " ";
    set<int> hclasses_set;
    vector<int> hclasses_vec;
    while(hclasses_vec.size() < has){
      //we want five digits here padding with 0s
      int num = random_generator() % numclasses;
      if (hclasses_set.find(num) == hclasses_set.end()) {
        hclasses_set.insert(num);
        hclasses_vec.push_back(num);
      }
    }

    stream << maxwantedclasses << endl;
    set<int> wclasses_set;
    vector<int> wclasses_vec;
    while(wclasses_vec.size() < maxwantedclasses){
      int r = random_generator() % numclasses;
      if (hclasses_set.find(r) == hclasses_set.end() && wclasses_set.find(r) == wclasses_set.end()){
        wclasses_set.insert(r);
        wclasses_vec.push_back(r);
      }
    }


    for ( int c : hclasses_vec) {
      stream << std::setfill('0') << std::setw(5) << c << ' ';
    }
    stream << endl;

    for (int c : wclasses_vec) {
      stream << std::setfill('0') << std::setw(5) << c << ' ';
    }
    stream << endl;
  }
}
