#include <sstream>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>

#include "menu.hpp"
#include "generator.hpp"

Database * generate_database() {
  int numclasses, numstudents, maxclasses, maxwantedclasses;
  std::cout << "Enter the number of classes: ";
  std::cin >> numclasses;
  std::cout << "Enter the number of students: ";
  std::cin >> numstudents;
  std::cout << "Enter the maximum number of classes on a students schedule: ";
  std::cin >> maxclasses;
  std::cout << "Enter the maximum number of classes a student might want: ";
  std::cin >> maxwantedclasses;

  std::stringstream generator_io;
  generate_scenario(generator_io, numclasses, numstudents, maxclasses, maxwantedclasses);

  generator_io >> numstudents;
  Database *database = new Database(numstudents);
  database->parse(generator_io, numstudents);
  return database;
}

Database * new_database() {
  return new Database(1000);
}

void print_database(Database *database) {
  if (!database) {
    std::cout << "No database exists, generate one!" << std::endl;
    return;
  }
  database->print();
}

void add_user(Database *database) {
  if (!database) {
    std::cout << "No database exists, generate one!" << std::endl;
    return;
  }

  std::stringstream io;
  std::string name;
  int num_has, num_wants;
  std::cout << "Enter a name: ";
  std::cin >> name;
  std::cout << "Enter the number of classes that " << name << " has to offer: ";
  std::cin >> num_has;
  std::cout << "Enter the number of classes that " << name << " wants: ";
  std::cin >> num_wants;
  io << name << ' ' << num_has << ' ' << num_wants << endl;

  std::string temp;
  std::cout << "Enter a space separated list of classes, from most to least hated, that " << name << " is offering\n"
            << "List of " << num_has << " classes: ";
  for (int i = 0; i < num_has; ++i) {
    std::cin >> temp;
    io << std::setfill('0') << std::setw(5) << temp << ' ';
  }
  io << std::endl;

  std::cout << "Enter a space separated list of classes, from most to least wanted, that " << name << " wants\n"
            << "List of " << num_wants << " classes: ";
  for (int i = 0; i < num_wants; ++i) {
    std::cin >> temp;
    io << std::setfill('0') << std::setw(5) << temp << ' ';
  }
  io << std::endl;

  database->add_student(io);
}

Database * read_database() {
  std::string filename;
  std::cout << "Enter the name of the file you want to read from: ";
  std::cin >> filename;
  std::fstream file(filename, std::fstream::in);
  if (file.fail()) {
    std::cout << "Error: could not open file '" << filename << "'" << std::endl;
    return nullptr;
  }

  int num_students;
  file >> num_students;
  Database *db = new Database(num_students+1000);
  while (num_students--) {
    db->add_student(file);
  }
  return db;
}


void start_class_matching_algorithm(Database *database) {
  if (!database) {
    std::cout << "No database exists, generate one!" << std::endl;
    return;
  }

  database->walk_through_swaps();
}

void menu() {
  int choice;
  Database *database = nullptr;

  while (true) {
    std::cout << "Main Menu: \n"
              << "  1) Generate a new database\n"
              << "  2) Start a database from scratch\n"
              << "  3) Read in a database from a file\n"
              << "  4) Print database\n"
              << "  5) Add a user\n"
              << "  6) Start Class-Matching Algorithm\n"
              << "  7) Exit\n"
              << "Enter Choice: ";

    std::cin >> choice;
    switch (choice) {

    case 1:
      delete database;
      database = generate_database();
      break;

    case 2:
      delete database;
      database = new_database();
      break;

    case 3:
      delete database;
      database = read_database();

    case 4:
      print_database(database);
      break;

    case 5:
      add_user(database);
      break;

    case 6:
      start_class_matching_algorithm(database);
      break;

    case 7:
      std::cout << "So long, and thanks for all the fish!" << std::endl;
      return;

    default:
      std::cout << "\n\nSorry, '" << choice << "' is not a valid option, try again\n"
                << "Enter Choice: ";
      std::cin >> choice;
      break;
    }
  }
}
