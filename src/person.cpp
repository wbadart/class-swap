/**
 * person.cpp
 *
 * Implementation and sheisse
 *
 * DEC 2016
 *
 */

#include "person.hpp"

Person::Person(std::string n, std::deque<std::pair<std::string, std::string>*> p)
        :netid(n), preferences(p){
    // ...
}

void Person::propose(Person p){
}

void Person::reject(void){
}

void Person::print_set(std::ostream& stream, std::map<std::string, Person> people){
    for(auto p: people){
        stream << p.second.netid << std::endl;
        for(auto c: p.second.preferences)
            stream << "\t" << c->first << ", " << c->second << std::endl;
        stream << std::endl;
    }
}

