#include "student.hpp"
#define get_trading_away_crn(wi) owner->trade_fors[owner->want_crns[wi]]

Course::Course(Student *o) : owner(o) {
}

Student::Student(std::string n, size_t num_has, size_t num_wants, Student *addr) : netid(n), trade_aways(num_has, Course(addr)), want_crns(num_wants) {
}

Student::Student() {
}

Course * Student::has(std::string crn) {
  for (auto course_iter = trade_aways.begin(); course_iter != trade_aways.end(); ++course_iter) {
    if (course_iter->crn == crn) {
      return &(*course_iter);
    }
  }
  return nullptr;
}

void Student::add_trade_for(Course *match) {
  // requires insertion, should not be a vector
  // like a map, but the keys are crns in order of preference and the values are lists of owners ordered by time

  // is there a better way of doing this?
  if (!trade_fors[match->crn].empty() && trade_fors[match->crn].back() == match) {
    return;
  }
  trade_fors[match->crn].push_back(match);
}

void Course::propose_to(Course *woman) {
  woman->received.push_back(this);
  last_proposal = woman;
}

bool Course::reject_all_but_best() {
  bool changed = false;

  if (received.empty()) {
    return false;
  }

  Course * best = received[0];
  for (Course *reception : received) {
    if (best == reception) {
      continue;
    }
    if (owner->more_preferred(reception, best)) {
      reject(best);
      best = reception;
      changed = true;
    }
    else {
      reject(reception);
      changed = true;
    }
  }

  received.clear();

  Course temp(owner);
  Course* pref;
  while (pref = temp.get_next_pref()) {
    if (pref == best) {
      received.push_back(best);
      return changed;
    }
  }
  reject(best);
  return true;
}

bool Course::walk_reject_all_but_best() {
  bool changed = false;

  if (received.empty()) {
    std::cout << "  " << owner->netid << " has not received a proposal for " << crn; std::cin.ignore();
    return false;
  }

  Course * best = received[0];
  for (Course *reception : received) {
    if (best == reception) {
      continue;
    }
    if (owner->more_preferred(reception, best)) {
      walk_reject(best);
      best = reception;
      changed = true;
    }
    else {
      walk_reject(reception);
      changed = true;
    }
  }

  received.clear();

  Course temp(owner);
  Course* pref;
  while (pref = temp.get_next_pref()) {
    if (pref == best) {
      received.push_back(best);
      std::cout << "  " << owner->netid << " does not reject " << owner->netid << "'s " << crn << " for " << best->owner->netid << "'s " << best->crn; std::cin.ignore();
      return changed;
    }
  }
  walk_reject(best);
  return true;
}

bool Student::more_preferred(Course *a, Course *b) {
  if (a == b) {
    return false;
  }

  for (size_t w = 0; w < want_crns.size(); w++) {
    auto trading_fors = trade_fors[want_crns[w]];
    for (size_t s = 0; s < trading_fors.size(); s++) {
      Course * c = trading_fors[s];
      if (c == a) {
        return true;
      }
      else if (c == b) {
        return false;
      }
    }
  }

  return false;
}

void Course::walk_reject(Course *proposer) {
  std::cout << "  " << owner->netid << " rejects " << owner->netid << "'s " << crn << " for " << proposer->owner->netid << "'s " << proposer->crn; std::cin.ignore();
  proposer->last_proposal = nullptr;
}

void Course::reject(Course *proposer) {
  proposer->last_proposal = nullptr;
}


Course * Course::get_next_pref() {
  if (want_crn_i >= owner->want_crns.size()) {
    return nullptr;
  }

  std::vector<Course*> prefs = get_trading_away_crn(want_crn_i);
  if(prefs.empty()){
    want_crn_i++;
    return get_next_pref();
  }
  Course *next_pref = prefs[student_i];

  if (++student_i == prefs.size()) {
    want_crn_i++;
    student_i = 0;
  }
  return next_pref;
}
