/**
 * main.cpp
 *
 * Driver program for class-swap marriage problem.
 *
 * DEC 2016
 *
 */

#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <getopt.h>

#include "constants.hpp"
#include "database.hpp"
#include "menu.hpp"

int main(int argc, char* argv[]){

  opterr = 0;

  char short_options[] {
    'h', // help
    'm', // menu
    'r', // read from stdin
    'c', // Check the stability of the swaps
    '\0' // end of string
  };


  static struct option long_options[] =
    {
      // {"help",         no_argument,       0, 'h'},
      // {"examples",     optional_argument, 0, 'e'},
      // {0, 0, 0, 0}
    };

  int option;
  int optionIndex = 0;
  // while there are still options to interpret
  while ((option = getopt_long(argc, argv, short_options, long_options, &optionIndex)) != -1)
    switch (option) {
    case 'h':
      std::cout << MSG::usage << std::endl;
      break;
    case 'm':
      menu();
      break;
    case 'r': {
      int num_students;
      while(std::cin >> num_students){
        Database students(num_students);
        students.parse(std::cin, num_students);
        students.generate_swaps();
        students.print_swaps();
      }
    }
      break;
    case 'c': {
      int num_students, found_unstable = 0;
      while(std::cin >> num_students){
        Database students(num_students);
        students.parse(std::cin, num_students);
        students.generate_swaps();
        bool check = students.check_swaps();
        std::cout << (check ? "Stable" : "Unstable") << std::endl;
        if(!check) found_unstable++;
      }
      if(found_unstable) return EXIT_FAILURE;
    }
      break;
    default:
      std::cerr << ERR::opt_not_recognized
                << " [ " << option << " ]";
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

