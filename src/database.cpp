/**
 * database.cpp
 *
 * Implementation of main class, Database. Please
 * see database.hpp for interface details.
 *
 * DEC 2016
 *
 */

#include "database.hpp"

Database::Database(int num_students) : students(num_students) {
}

void Database::add_student(std::istream& input) {
  static int num_students = 0;

  size_t num_has, num_wants;
  std::string netid;
  Student *new_student;

  input >> netid >> num_has >> num_wants;
  // build the new student
  students[num_students] = Student(netid, num_has, num_wants, &students[num_students]);
  new_student = &students[num_students];
  ++num_students;

  // account for their haves
  for (size_t j = 0; j < num_has; j++) {
    input >> new_student->trade_aways[j].crn;
    new_student->trade_aways[j].owner = new_student;
  }

  for (size_t j = 0; j < num_wants; j++) {
    input >> new_student->want_crns[j];
  }

  process_student(new_student);
}

void Database::parse(std::istream& input, int num_students) {
  size_t num_has, num_wants;
  std::string netid;
  Student *new_student;

  for (size_t student_index = 0; student_index < num_students; ++student_index) {
    input >> netid >> num_has >> num_wants;
      // build the new student
    students[student_index] = Student(netid, num_has, num_wants, &students[student_index]);
    new_student = &students[student_index];

    // account for their haves
    for (size_t j = 0; j < num_has; j++) {
      input >> new_student->trade_aways[j].crn;
    }

    for (size_t j = 0; j < num_wants; j++) {
      input >> new_student->want_crns[j];
    }

    process_student(new_student);
  }
}

void Database::generate_swaps() {
  bool changed = true;
  while (changed) {
    changed = false;
    // do proposals
    for (Student& student : students) {
      for(auto& c: student.trade_aways){
        if(!c.last_proposal){
          Course *next_pref = c.get_next_pref();
          if (next_pref) {
            c.propose_to(next_pref);
            changed = true;
          }
        }
      }
    }
    // do rejections
    for (Student& student: students) {
      for(auto& c: student.trade_aways){
        changed = c.reject_all_but_best() || changed;
      }
    }
  }
}

void Database::walk_through_swaps() {
  bool changed = true;
  int round = 1;
  std::cin.ignore();
  while (changed) {
    std::cout << "\n-- Round " << round << " --" << std::endl;
    ++round;

    changed = false;
    // do proposals
    std::cout << "Proposals:" << std::endl;
    for (Student& student : students) {
      for(auto& c: student.trade_aways){
        if(!c.last_proposal){
          Course *next_pref = c.get_next_pref();
          if (next_pref) {
            std::cout << "  " << student.netid << " proposes " << c.crn << " for " << next_pref->owner->netid << "'s " << next_pref->crn; std::cin.ignore();
            c.propose_to(next_pref);
            changed = true;
          }
          else {
            std::cout << "  " << student.netid << " has run out of options for " << c.crn; std::cin.ignore();
          }
        }
        else {
          std::cout << "  " << student.netid << " maintains their proposal of " << c.crn << " for " << c.last_proposal->owner->netid << "'s " << c.last_proposal->crn; std::cin.ignore();
        }
      }
    }
    // do rejections
    std::cout << "\nRejections:" << std::endl;
    for (Student& student: students) {
      for(auto& c: student.trade_aways){
        changed = c.walk_reject_all_but_best() || changed;
      }
    }
  }
  std::cout << "\nNothing changed, the swaps are complete!"; std::cin.ignore();
}


void Database::print_swaps() {
  for (auto& student : students) {
    if (student.netid == "") {
      return;
    }
    std::cout << student.netid << std::endl;
    for (Course& course : student.trade_aways) {
      // std::cout << (course->last_proposal ? course->last_proposal->crn : course->crn) << " ";
      if (!course.last_proposal) {
        std::cout << course.crn << " ";
      }
      else{
        std::cout << course.last_proposal->crn << " ";
        std::cerr << "(traded " << course.crn << " to " << course.last_proposal->owner->netid << ")    ";
      }
    }
    std::cout << std::endl;
  }
}

bool Database::check_swaps() {
  bool stable = true;
  std::cerr << "-- Checking Swap Stability --" << std::endl;
  for (auto& student : students) {
    if (student.netid == "") {
      return stable;
    }
    std::cerr << student.netid << ":" << std::endl;
    for (auto& trade_away : student.trade_aways) {
      std::cerr << "  " << trade_away.crn << ':' << std::endl;
      for (auto want_crn : student.want_crns) {
        for (Course *trade_for : student.trade_fors[want_crn]) {
          std::cerr << "    " << trade_for->crn << ": ";
          if (student.more_preferred(trade_for, trade_away.last_proposal) &&
              trade_for->owner->more_preferred(&trade_away, trade_for->last_proposal)) {
            std::cerr << "Yes" << std::endl;
            stable = false;
          }
          else {
            std::cerr << "No" << std::endl;
          }
        }
      }
    }
  }
  return stable;
}


void Database::process_student(Student *new_student) {
  // for each of their wants

  for (std::string current_want_crn : new_student->want_crns) {
    new_student->trade_fors[current_want_crn] = std::vector<Course *>();

    // for every old student
    for (auto old_student_iter = students.begin(); old_student_iter != students.end(); old_student_iter++) {
      Student *old_student = &(*old_student_iter);

      // Don't consider the same student
      if (new_student == old_student) {
        break;
      }

      // If old has what new wants
      Course *match_for_new = old_student->has(current_want_crn);
      if (match_for_new) {

        // and old wants what new has
        for (std::string old_want_crn : old_student->want_crns) {
          Course *match_for_old = new_student->has(old_want_crn);
          if (match_for_old) {

            // update their trade_fors
            // room for improvement, always repeating one
            new_student->add_trade_for(match_for_new);
            old_student->add_trade_for(match_for_old);
          }
        }
      }
    }
  }
}

void Database::print() {
  for (auto student : students) {
    if (student.netid == "") {
      return;
    }

    std::cout << student.netid << std::endl;

    std::cout << "Trade Aways: ";
    for (auto& course : student.trade_aways) {
      std::cout << course.crn << ", ";
    }
    std::cout << std::endl;

    std::cout << "Trade Fors:" << std::endl;
    for (auto crn : student.want_crns) {
      std::cout << "  " << crn << ": ";
      for (Course *course : student.trade_fors[crn]) {
        std::cout << course->owner->netid << ", ";
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }
}

void Database::clear() {
  students.clear();
}
