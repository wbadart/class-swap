/**
 * database.hpp
 *
 * Interface of our main class, Database. Provides
 * methods to parse list of students and generate
 * stable pairings.
 *
 * DEC 2016
 *
 */

#ifndef H_DATABASE
#define H_DATABASE

#include <deque>
#include <iostream>
#include <iterator> // std::distance
#include <map>
#include <set>
#include <string>
#include <vector>

#include "student.hpp"

class Database {
  friend std::ostream& operator<<(std::ostream&, const Database&);

public:
  Database(int num_students);
  void parse(std::istream& input, int num_students);
  void generate_swaps();
  void print();
  void print_swaps();
  bool check_swaps();
  void walk_through_swaps();
  void process_student(Student *student);
  void clear();
  void add_student(std::istream& input);
private:
  std::vector<Student> students;
};

#endif // H_DATABASE

