#!/usr/bin/env python2.7

'''
' insert_marvel_reference_here.py
'
' Forking HTTP server that communiactes via stdio
' with an arbitrary executable. Based on (i.e. copied
' almost directly from) gwen.py  by Peter Bui.
'
' DEC 2016
'
'''

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer   import ForkingMixIn

import getopt
import json
import logging
import mimetypes
import os
import subprocess
import sys

# Constants

WWW_PORT = 9001
WWW_ROOT = os.path.abspath('www')

# Utility Functions

def obj2stdin(students):
    result = str(len(students)) + '\n'
    for s in students:
        result += '{} {} {}\n'.format(s[0], len(s[1]), len(s[2]))
        result += reduce(lambda s, x: s + x + ' ', s[1], '')
        result += '\n'
        result += reduce(lambda s, x: s + x + ' ', s[2], '')
        result += '\n'
    return result

def determine_mimetype(url):
    return mimetypes.guess_type(url)[0] or 'text/plain'

# Handlers

class ForkingHTTPServer(ForkingMixIn, HTTPServer):
    pass

class WWWHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/':
            self.path = '/index.html'

        mimetype = determine_mimetype(self.path)
        realpath = os.path.abspath(WWW_ROOT + self.path)

        if not realpath.startswith(WWW_ROOT):
            self.send_error(404, 'Invalid file path: {}'.format(self.path))
            return

        try:
            with open(realpath, 'rb') as fh:
                self.send_response(200)
                self.send_header('Content-type', mimetype)
                self.end_headers()
                self.wfile.write(fh.read())
        except IOError as e:
            self.send_error(404, 'File Not Found: {}\n{}'.format(self.path, e))

    def do_POST(self):
        data = json.loads(self.rfile.read(int(self.headers.getheader('content-length'))))
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

        print obj2stdin(data)

        p = subprocess.Popen(['./class_swap', '-r'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        d = p.communicate(input=obj2stdin(data))
        json.dump(d[0], self.wfile) # d = (stdout, stderr)

# Usage

def usage(exit_code):
    print >>sys.stderr, 'usage: {} -d WWW_ROOT -p WWW_PORT'.format(os.path.basename(sys.argv[0]))
    sys.exit(exit_code)

# Main Execution

if __name__ == '__main__':

    try:
        options, arguments = getopt.getopt(sys.argv[1:], "hd:p:")
    except getopt.GetoptError as e:
        usage(1)
    for option, value in options:
        if option == '-h':
            usage(0)
        elif option == '-d':
            WWW_ROOT = value
        elif option == '-p':
            WWW_PORT = int(value)
        else:
            usage(1)

    server = ForkingHTTPServer(('0.0.0.0', WWW_PORT), WWWHandler)
    print 'Started HTTP Server on port', WWW_PORT
    server.serve_forever()

