Class Swap
==========

When registering for classes, it's rare to get your perfect schedule
on the first try, and you end up with at least a couple classes that
you're not too stoked about. But, as the old adage goes, one student's
garbage class in another student's treasure. Class Swap pairs up
students who ended up in classes that their partner wants and they
don't.

Usage
-----

To build simply run `make`. Usage instructions are provided below. To
run the web application, run `./insert_marvel_reference_here.py` and
visit `http://localhost:9001` in your favored browser. See below for
usage details.

**Core program:**
```
$ ./class_swap -h
usage: class_swap [ OPTIONS ]
    Reads stdin for network of students and class preferences. Outputs pairs of
    valid and stable swaps.

    OPTIONS
    -h          Display this help message
    -m          Use the interactive menu to play around
    -c          Verify pairing stability
    -r          Read a scenario from stdin and output the swapped schedules
```

**Web app:**
```
$ ./insert_marvel_reference_here.py -h
usage : insert_marvel_reference_here.py -d WWW_ROOT -p WWW_PORT
```

Team
----

- Conrad Bailey *cbailey8*
- Madeline Kusters *mkusters*
- Will Badart *wbadart*

See `CONTRIBUTORS.md` for details.

