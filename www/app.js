/**
 * app.js
 *
 * Programatic control of web interface of Class
 * Swap. Built on a simple angular module.
 *
 * DEC 2016
 *
 */

(function(){
'use strict';

var STUDENTS = {
    'cbailey8': [
        ['12345', '54321'],
        ['18302']],
    'mkusters':[
        ['01832', '10932', '12312'],
        ['18402', '10321']],
    'wbadart':[
        ['19302'],
        ['04912']]
};

angular.module('App', [])
    .controller('MainController', MainController)
    .constant('API_HOST', 'localhost:9001');

MainController.$inject = ['$http', 'API_HOST'];
function MainController($http, API_HOST){

    var vm         = this;
    // vm.students    = STUDENTS;
    vm.students    = [];
    vm.new_student = clear_student;
    vm.new_g       = '';
    vm.new_t       = '';

    vm.del = function(n){vm.students.splice(n, 1);};

    vm.add_student   = add_student;
    vm.clear_student = clear_student;
    vm.submit        = submit;

    init();
    function init(){
        $http.get('/sample-input.txt')
            .then(populate_students)
            .catch(cb_err);
    }

    function populate_students(res){
        var data = res.data.split('\n').slice(1).filter(function(s){return !!s;});
        for(var i = 0; i < data.length; i += 3)
            add_student([
                data[i].split(' ')[0],
                data[i + 1].split(' '),
                data[i + 2].split(' ')
            ]);
    }

    function add_student(s){
        vm.students.push([s[0], s[1], s[2]]);
        clear_student();
    }

    function clear_student(){
        return (vm.new_student = ['', [], []]);
    }

    function submit(students){
        console.log(students);
        $http.post('http://' + API_HOST, students)
            .then(on_get_swaps, cb_err);
    }

    function on_get_swaps(res){
        // res.data = JSON.parse(res.data);
        var data = res.data.split('\n').filter(
            function(s){ return !!s; });
        console.log(data);
        vm.got = true;
        var tmp = data
            .filter(function(e, i){
                return !(i % 2);})
            .reduce(function(a, d, i){
                a.push([d, data[2 * i + 1]]);
                return a;}, []);
        console.log(tmp);
        vm.students = vm.students.map(function(s, i){
            s.push(tmp[i][1].split(' '));
            console.log(s);
            return s;
        });
        // for(var i = 0; i < data.length; i += 2);
            // vm.students[i].push(data[i + 1].split(' '));
    }

    function cb_err(res){
        alert('An error occurred.');
        console.error(res);
    }
}

})();

